import { createSlice } from '@reduxjs/toolkit';

import initialState from './initialState';
import reducers from './reducers';

export const usersSlice = createSlice({
  name: 'user',

  initialState: initialState,

  reducers: reducers,
});

// action types
export const {
  fetchUsersPending,
  fetchUsersSuccess,
  fetchUsersFailure
} = usersSlice.actions;

export default usersSlice.reducer;

