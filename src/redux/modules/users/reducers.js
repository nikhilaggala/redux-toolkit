const reducers =  {
  fetchUsersPending: state => {
    state.loading = {
      ...state.loading,
      fetchUsers: true
    }
  },

  fetchUsersSuccess: (state, action) => {
    const { payload } = action;

    state.loading = {
      ...state.loading,
      fetchUsers: false
    }

    state.users = payload.users;
  },

  fetchUsersFailure: state => {
    state.loading = {
      ...state.loading,
      fetchUsers: false
    };

    state.error = {
      ...state.error,
      fetchUsers: false
    }
  },

};

export default reducers;
