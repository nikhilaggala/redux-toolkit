import * as actionTypes from './index';

export const fetchUsers = () => dispatch => {
  dispatch(actionTypes.fetchUsersPending());

  fetch('https://jsonplaceholder.typicode.com/posts/')
  .then(response => {
    if (!response.ok) return dispatch(actionTypes.fetchUsersFailure());

    return response.json();
  })
  .then(data => {
    return dispatch(actionTypes.fetchUsersSuccess({ users: data }));
  });
};

