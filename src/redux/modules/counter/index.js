import { createSlice } from '@reduxjs/toolkit';

import initialState from './initialState';
import reducers from './reducers';

export const counterSlice = createSlice({
  name: 'counter',

  initialState: initialState,

  reducers: reducers,
});

// action types
export const {
  increment,
  decrement,
  incrementByAmount
} = counterSlice.actions;

export default counterSlice.reducer;

