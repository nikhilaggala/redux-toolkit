import * as actionTypes from './index';

export const incrementAsync = amount => dispatch => {
  setTimeout(() => {
    dispatch(actionTypes.incrementByAmount(amount));
  }, 1000);
};

export const selectCount = state => state.counter.value;
