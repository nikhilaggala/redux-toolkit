import { configureStore } from '@reduxjs/toolkit';
import counterReducer from './modules/counter';
import usersReducer from './modules/users';

export default configureStore({
  reducer: {
    counter: counterReducer,
    users: usersReducer
  },
});
