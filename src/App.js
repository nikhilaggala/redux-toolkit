import React from 'react';
import { useDispatch } from 'react-redux';

import {
  fetchUsers
} from './redux/modules/users/actionCreators';

import logo from './logo.svg';
import { Counter } from './features/counter/Counter';
import './App.css';


function App() {
  const dispatch = useDispatch();

  return (
    <div className="App">
      <header className="App-header">
        <div onClick={() => dispatch(fetchUsers())}>Click me </div>
        <img src={logo} className="App-logo" alt="logo" />
      </header>
    </div>
  );
}

export default App;
